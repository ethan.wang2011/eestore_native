react native docs, feel free to look up any here:
https://facebook.github.io/react-native/docs/tutorial.html

To run this app
1. install XCode on your mac
2. install react-native: 
   * brew install node
   * npm install -g react-native-cli
   * cd "/{path to eestore_native}"
3. to run app on simulator:
   * react-native run-ios

very simple to start 
